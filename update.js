#! /usr/bin/env node
var lzString = require('lz-string');
var fs = require('fs');

var path = "data_work/spapi/"
var dist_path = "data_work/spapi/dist/"

var token = "3538ddbb62c79a06a6bdd97e9f4b881b";
var competition_id = 13 // 13 - world cup, 11 - CL
var list_url = "https://spapi.pw/api/v1/competitions/" 
	+ competition_id 
	+ "/finished_matches?access_token="
	+ token
	

var list = JSON.parse(fs.readFileSync(path + "matches.json"))	

console.log("pages: " + list.total_pages)
console.log("entries: " + list.total_count)

list.items.forEach( function(d) {
	console.log(d)

	var id = 62588;
	
	if (d.home_team.abbreviation_name == "SAU")
		d.home_team.abbreviation_name  = "KSA";
	if (d.away_team.abbreviation_name == "SAU")
		d.away_team.abbreviation_name  = "KSA";

		
	var name = d.home_team.abbreviation_name + "-" + d.away_team.abbreviation_name;

	var actions = JSON.parse(fs.readFileSync(path + id + "-actions.json", 'utf8'));
	var details = JSON.parse(fs.readFileSync(path + id + "-details.json", 'utf8'));
	var statistics = JSON.parse(fs.readFileSync(path + id + "-statistics.json", 'utf8'));
	
	var quick_info = {
		home: d.home_team,
		away: d.away_team,
		score: d.scores_and_stats.scores.home + ":" + d.scores_and_stats.scores.away
	}

	var game_data = Object.assign( {}, actions, 
		{ details: details }, 
		{ statistics: statistics }, 
		{ quick_info: quick_info});	
	
	console.log(quick_info);
	fs.appendFile(dist_path + name + ".json", 
		JSON.stringify(game_data), 
		function(err) {
		if(err) {
			console.log(err);
		} else {
			console.log(dist_path + name + ".json " + "was saved!");
		}
    }); 
    
    fs.appendFile(dist_path + name, 
		lzString.compressToEncodedURIComponent(JSON.stringify(game_data)), 
		function(err) {
		if(err) {
			console.log(err);
		} else {
			console.log(dist_path + name + " was saved!");
		}
    }); 
    
    console.log(lzString.compressToEncodedURIComponent(JSON.stringify(game_data)));

    
	    
});

	
//console.log(lzString.compressToEncodedURIComponent(JSON.stringify(game_data)));
//console.log(actions);



//console.log(lzString.compressToEncodedURIComponent(actions, { encoding: 'utf8', })));



