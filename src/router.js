import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Contact from './views/Contact.vue'
import Donate from './views/Donate.vue'

import GameView from  './views/GameView.vue'
import TournamentStats from "./views/TournamentStats.vue"
import NotFound from  './components/NotFound.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
    },
    {
      path: '/donate',
      name: 'donate',
      component: Donate
    },
    {
      path: '/gameview/:id',
      name: 'gameview',
      component: GameView,
      props:true
    },
    {
      path: '/tournamentstats',
      name: 'tournamentstats',
      component: TournamentStats,
      props:true
    },
     { path: '*', component: NotFound}
  ],

  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return {selector: to.hash}
    } else {
      if (savedPosition) {
        return savedPosition
      } else {
        return { x: 0, y: 0 }
      }
    }
  },
})
