import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueResource from 'vue-resource'
import VueScrollTo from 'vue-scrollto'
import SocialSharing from 'vue-social-sharing'

//import VueMaterial from 'vue-material'
import { MdButton, MdDrawer, MdIcon, MdToolbar, MdList, MdContent, MdField, MdMenu, MdDivider} from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'

Vue.use(SocialSharing);

Vue.use(MdContent)
Vue.use(MdList)
Vue.use(MdToolbar)
Vue.use(MdButton)
Vue.use(MdDrawer)
Vue.use(MdIcon)
Vue.use(MdContent)
Vue.use(MdField)
Vue.use(MdMenu)
Vue.use(MdDivider)

Vue.use(VueResource);
Vue.use(VueScrollTo)

Vue.config.productionTip = false


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
