
export default (el, width, height) => {
  var element = el.append("g")
    .attr("class","field")

  element
    .append("rect")
      .attr("x", 0)
      .attr("y", 0)
      .attr("width", width)
      .attr("height", height)
      .attr("stroke","lightgrey")
      .attr("fill","white")


  var r = width/ (105/9.15);
  element
    .append("path")
      .attr( "d", "M" + (width - width/(105/11)) + ","+ (height/2 - r) +" a1,1 0 1,0 0," + 2* r)
      .attr("fill", "none")
      .attr("stroke", "lightgrey")

  element
    .append("path")
      .attr( "d", "M" + (width/(105/11)) + ","+ (height/2 - r) +" a1,1 0 0,1 0," + 2* r)
      .attr("fill", "none")
      .attr("stroke", "lightgrey")

  element
    .append("rect")
      .attr("x", 0)
      .attr("y", (height/2 - (width/ (105/20.16))))
      .attr("width", (width/ (105/16.5)))
      .attr("height", (width/ (105/40.32)))
      .attr("stroke","lightgrey")
      .attr("fill","white")

  element
    .append("rect")
      .attr("x", width - (width/(105/16.5)))
      .attr("y", (height/2 - (width/(105/20.16))))
      .attr("width", (width/(105/16.5)))
      .attr("height", (width/(105/40.32)))
      .attr("stroke","lightgrey")
      .attr("fill","white")

  element
    .append("rect")
      .attr("x", 0)
      .attr("y", (height/2 - (width/ (105/9.16))))
      .attr("width", (width/ (105/5.5)))
      .attr("height", (width/ (105/18.32)))
      .attr("stroke","lightgrey")
      .attr("fill","none")

  element
    .append("rect")
      .attr("x", -(width/ 105))
      .attr("y", (height/2 - (width/ (105/3.66))))
      .attr("width", (width/ 105))
      .attr("height", (width/ (105/7.32)))
      .attr("stroke","lightgrey")
      .attr("fill","none")

  element
    .append("rect")
      .attr("x",width)
      .attr("y", (height/2 - (width/ (105/3.66))))
      .attr("width", (width/ 105))
      .attr("height", (width/ (105/7.32)))
      .attr("stroke","lightgrey")
      .attr("fill","none")



  element
    .append("rect")
      .attr("x", width - (width/(105/5.5)))
      .attr("y", (height/2 - (width/ (105/9.16))))
      .attr("width", (width/ (105/5.5)))
      .attr("height", (width/ (105/18.32)))
      .attr("stroke","lightgrey")
      .attr("fill","none")

  element
    .append("line")
      .attr("x1", width/2)
      .attr("y1", 0)
      .attr("x2", width/2)
      .attr("y2", height)
      .attr("stroke","lightgrey")
      .attr("fill","none")


  element
    .append("circle")
      .attr("cx", width/2)
      .attr("cy", height/2)
      .attr("r", width/ (105/9.15))
      .attr("stroke","lightgrey")
      .attr("fill","none")


    return element;
};
