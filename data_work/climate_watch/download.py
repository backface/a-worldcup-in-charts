#!/bin/python

import csv
import json
import requests
import time
import os

headers={'referer': "http://www.climatewatchdata.org"}

with open("../countries.csv") as csvfile:
	reader = csv.DictReader(csvfile)
	for row in reader:
		
		ofile = row["fifa_code"] + ".json"
		if not os.path.isfile(ofile):
			print "download data for " + row["name"]
			link = "https://www.climatewatchdata.org/api/v1/emissions?gas=131&location=" + row["iso"] + "&source=31"
			r = requests.get(link, headers)
			with open(ofile,"w") as f:
				f.write(r.text)
				f.close()
				
			time.sleep(1)
