#!/bin/python

import csv
import json
import requests
import time
import os
import json
import math

headers={'referer': "http://www.climatewatchdata.org"}

with open("../countries.csv") as csvfile:
	reader = csv.DictReader(csvfile)
	writer = csv.DictWriter(open("currents.csv","w"), fieldnames=["fifa_code","ghg_total"])
	writer.writeheader()
	
	for row in reader:
		
		ofile = row["fifa_code"] + ".json"
		print "download data for " + row["name"]
		#link = "https://www.climatewatchdata.org/api/v1/emissions?gas=131&location=" + row["iso"] + "&source=32"
		
		#r = requests.get(link, headers)
		
		j = json.load(open("PIK/{}.json".format(row["fifa_code"])))
		
		maxv = 0
		for i in j[0]["emissions"]:
			maxv = max(maxv, i["value"])
		
		print maxv
		orow = { 
			"fifa_code": row["fifa_code"], 
			"ghg_total": maxv
		}
		writer.writerow(orow)


				
			#time.sleep(1)
