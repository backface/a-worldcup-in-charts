#!/bin/sh

wget -d --header="User-Agent: Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11" \
	--header="Referer: http://data.footprintnetwork.org/" \
	--header="Accept-Encoding: compress, gzip" \
	--header="Accept: application/json" \
	$1 $2 $3 $4 $5
