

f="FIFA World Cup 2018.xml"

import csv
import untangle
obj = untangle.parse(f)
teams = obj.PlayerInfos.Sports.Sport.Leagues.League.Teams.Team;


countries = {}
fields = ["fifa_code", "iso", "name", "alt_name" , "capital", "color1", "color2"]

with open("../countries.csv","r") as fd:
	reader = csv.DictReader(fd)
	for row in reader:
		countries[row["name"]] = row
		
	
csvout = open("countries.csv","w")
writer = csv.DictWriter(csvout,fieldnames=fields)
writer.writeheader()

for t in teams:
	color1 = t.ColorInfo.ColorSet1.PrimaryColor
	sec1 = t.ColorInfo.ColorSet1.HightlightColor
	color2 = t.ColorInfo.ColorSet2.PrimaryColor
	sec2 = t.ColorInfo.ColorSet1.HightlightColor
	countries[t["Name"]]["color1"] = '#%02x%02x%02x' % (int(color1["r"]), int(color1["g"]), int(color1["b"]))
	countries[t["Name"]]["color2"] = '#%02x%02x%02x' % (int(color2["r"]), int(color2["g"]), int(color2["b"]))
	writer.writerow(countries[t["Name"]])

