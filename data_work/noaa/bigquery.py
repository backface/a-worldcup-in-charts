from google.cloud import bigquery
client = bigquery.Client()

sql = """
	SELECT value6 / 100 AS temp, year, name
	FROM `bigquery-public-data.ghcn_m.ghcnm_tavg` a \
	JOIN `bigquery-public-data.ghcn_m.ghcnm_tavg_stations` b \
	ON  b.id = a.id  \
	WHERE b.name LIKE 'WIEN%' AND value6 != -9999 \
	ORDER BY year DESC
    """

df = client.query(sql).to_dataframe()
df.to_csv("Wien-June-Avg.csv")

# find nearest station
"""
	SELECT
	id,
	name,
	state,
	latitude,
	longitude
	FROM
	`bigquery-public-data.ghcn_d.ghcnd_stations`
	WHERE
	latitude > 41.7
	AND latitude < 42
	AND longitude > -87.7
	AND longitude < -87.5
"""



lat = 44.1a
lon = 16.2

"""
	SELECT 
		id, 
		( 3959 * acos( cos( radians(""" + lat + """) ) * cos( radians( lat ) ) * 
		cos( radians( lng ) - radians(""" + lon + """) ) + sin( radians(""" + lat + """) ) * 
		sin( radians( lat ) ) ) ) AS distance 
	FROM 
		your_table_name 
	HAVING
		distance < 25 
	ORDER BY 
		distance 
	LIMIT 0 , 20;
"""
