
const request = require('request');
const lzString = require('lz-string');
const fs = require('fs');
var sync = require('sync-request');

const download = false;

var replacements = [
	["MOR","MAR",],
	["SAU","KSA",],
	["SPA","ESP",],
	["ICE","ISL",],	
	["NIG","NGA",],
    ["COS","CRC",],
    ["SER","SRB",],
	["SWI","SUI",],
	["SOU","KOR",],
	["JAP","JPN",],
]

var path = "./match_data/"
var dist_path = "dist/"
var token = "3538ddbb62c79a06a6bdd97e9f4b881b";
var competition_id = 13 // 13 - world cup, 11 - CL
var list_url = "https://spapi.pw/api/v1/competitions/"+ competition_id 	+ "/finished_matches?per_page=20&access_token=" 	+ token

	
console.log("downloading finished matches for competition " + competition_id);


if (download) 
	fs.writeFileSync(path + "matches.json", sync('GET',list_url).getBody())
	
var list = JSON.parse(fs.readFileSync(path + "matches.json"))	

var goals = [];
var shots = [];


console.log(list.total_count + " games (" + list.total_pages + ")")

if (list.total_pages > 1 && download) {
	for (var i=2; i <= list.total_pages; i++) {
		console.log("getting page 2")
		fs.writeFileSync(path + "matches-page-" + i + ".json", sync('GET',list_url + "&page=" + i).getBody())
		var addlist = JSON.parse(fs.readFileSync(path + "matches-page-" + i + ".json"))	
		addlist.items.forEach( function(d) { 
			list.items.push(d);
			//console.log("adding " + d.home_team.abbreviation_name + " - " + d.away_team.abbreviation_name );
		})
	}
}


list.items.forEach( function(d) {

	var id = d.guid;		

	replacements.forEach(function (r) {
		if (d.home_team.abbreviation_name == r[0])
			d.home_team.abbreviation_name = r[1]
		if (d.away_team.abbreviation_name == r[0])
			d.away_team.abbreviation_name = r[1]
	})

	var name = d.home_team.abbreviation_name + "-" + d.away_team.abbreviation_name;
	var prod_file = dist_path + name;
	

	if (!fs.existsSync(prod_file)) {
		var url_details = "https://spapi.pw/api/v1/matches/" + d.guid + "/details?access_token=" + token;
		var url_stats = "https://spapi.pw/api/v1/matches/"+ d.guid + "/statistics?access_token=" + token;
		var url_actions =  "https://spapi.pw/api/v1/matches/"+ d.guid + "/actions?access_token="  + token;

		var file_details = path + name + "-" + d.guid + "-details.json";
		var file_stats = path + name + "-" + d.guid + "-statistics.json";
		var file_actions = path + name + "-" + d.guid + "-actions.json";

		if (!fs.existsSync(file_details)) {
			console.log("downloading details for " + name + " (" + d.guid + ")");
			fs.writeFileSync(file_details, sync('GET',url_details).getBody())
		}
		
		if (!fs.existsSync(file_actions)) {
			console.log("downloading actions for " + name + " (" + d.guid + ")");
			fs.writeFileSync(file_actions, sync('GET',url_actions).getBody())
		}

		if (!fs.existsSync(file_stats)) {
			console.log("downloading statistics for " + name + " (" + d.guid + ")");
			fs.writeFileSync(file_stats, sync('GET',url_stats).getBody())
		}

		var actions = JSON.parse(fs.readFileSync(file_actions, 'utf8'));
		var details = JSON.parse(fs.readFileSync(file_details, 'utf8'));
		var stats = JSON.parse(fs.readFileSync(file_stats, 'utf8'));
		
		goals = goals.concat(actions.actions.filter( function(d) { return ["Goal"].indexOf(d.type.name) > -1  && !d.hasOwnProperty('OwnGoal') } ));
		shots = shots.concat(actions.actions.filter( d => ["SavedShot","MissedShots","ShotOnPost","Goal"].indexOf(d.type.name) > -1  && !d.hasOwnProperty('OwnGoal') ));
			
		var quick_info = {
			home: d.home_team,
			away: d.away_team,
			score: d.scores_and_stats.scores.home + ":" + d.scores_and_stats.scores.away
		}

		replacements.forEach(function (d) {
			if (quick_info.home.abbrevaition_name == d[0])
				quick_info.home.abbrevaition_name = d[1]
			if (quick_info.away.abbrevaition_name == d[0])
				quick_info.away.abbrevaition_name = d[1]	
		})
		
		var game_data = Object.assign({}, actions, { details: details}, { statistics: stats }, { quick_info: quick_info});
		
	
		fs.writeFileSync(prod_file,
			lzString.compressToEncodedURIComponent(JSON.stringify(game_data)), 
			function(err) {
			if(err) {
				console.log(err);
			} else {
				console.log(dist_path + name + " was saved!");
			}
		}); 
	} else {
		
		console.log(name + " exits");
		var file_actions = path + name + "-" + d.guid + "-actions.json";
		var actions = JSON.parse(fs.readFileSync(file_actions, 'utf8'));
		
		goals = goals.concat(actions.actions.filter( function(d) { return ["Goal"].indexOf(d.type.name) > -1  && !d.hasOwnProperty('OwnGoal') } ));
		shots = shots.concat(actions.actions.filter( d => ["SavedShot","MissedShots","ShotOnPost","Goal"].indexOf(d.type.name) > -1  && !d.hasOwnProperty('OwnGoal') ));
	}	
});


fs.writeFileSync(path + "matches.json", JSON.stringify(list))

fs.writeFileSync(path + "all_goals.json", 
	JSON.stringify(goals), 
	function(err) {
	if(err) {
		console.log(err);
	} else {
		console.log(path + "all_goals.json was saved!");
	}
}); 

fs.writeFileSync(path + "all_shots.json", 
	JSON.stringify(shots), 
	function(err) {
	if(err) {
		console.log(err);
	} else {
		console.log(path + "all_shots.json was saved!");
	}
}); 

fs.writeFileSync(dist_path + "all_shots.7",
	lzString.compressToEncodedURIComponent(JSON.stringify(shots)), 
	function(err) {
	if(err) {
		console.log(err);
	} else {
		console.log(dist_path + "all_shots.7 was saved!");
	}
}); 

	
fs.writeFileSync(dist_path + "007",
	lzString.compressToEncodedURIComponent(JSON.stringify(list)), 
	function(err) {
	if(err) {
		console.log(err);
	} else {
		console.log(dist_path + name + " was saved!");
	}
}); 


const { execSync } = require('child_process');
execSync('cp dist/* ../../public/data/f/');



