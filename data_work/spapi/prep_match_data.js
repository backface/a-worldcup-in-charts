#! /usr/bin/env node
var lzString = require('lz-string');
var fs = require('fs');

var quick_info = {
	home: {
		name: "Brazil",
		abrevation: "BRA",
	},
	away: {
		name: "Germany",
		abrevation: "GER",
	},
	score: "0:0",
}

var actions = JSON.parse(fs.readFileSync('62588-actions.json', 'utf8'));
var details = JSON.parse(fs.readFileSync('62588-details.json', 'utf8'));
var statist = JSON.parse(fs.readFileSync('62588-statistics.json', 'utf8'));
var game_data = Object.assign({}, actions, { details: details}, { statistics: statist }, { quick_info: quick_info});
console.log(lzString.compressToEncodedURIComponent(JSON.stringify(game_data)));
