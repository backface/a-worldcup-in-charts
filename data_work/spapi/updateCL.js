
const request = require('request');
const lzString = require('lz-string');
const fs = require('fs');
var sync = require('sync-request');

var replacements = [
	["MOR","MAR",],
	["SAU","KSA",],
	["SPA","ESP",],
]

var path = "./CL/match_data/"
var dist_path = "./CL/dist/"
var token = "3538ddbb62c79a06a6bdd97e9f4b881b";
var competition_id = 11 // 13 - world cup, 11 - CL
var list_url = "https://spapi.pw/api/v1/competitions/"+ competition_id 	+ "/finished_matches?access_token=" 	+ token

	
	console.log("downloading finished matches for competition " + competition_id);
	
	fs.writeFileSync(path + "matches.json", sync('GET',list_url).getBody())
	var list = JSON.parse(fs.readFileSync(path + "matches.json"))	
	
	fs.writeFileSync(dist_path + "011",
		lzString.compressToEncodedURIComponent(JSON.stringify(list)), 
		function(err) {
		if(err) {
			console.log(err);
		} else {
			console.log(dist_path + name + " was saved!");
		}
	}); 

	console.log(list.total_count + " games (" + list.total_pages + ")")

	list.items.forEach( function(d) {

		var id = d.guid;
		

		replacements.forEach(function (r) {
			if (d.home_team.abbreviation_name == r[0])
				d.home_team.abbreviation_name = r[1]
			if (d.away_team.abbreviation_name == r[0])
				d.away_team.abbreviation_name = r[1]
		})

		var name = d.home_team.abbreviation_name + "-" + d.away_team.abbreviation_name;
		var prod_file = dist_path + name;
		

		if (!fs.existsSync(prod_file)) {
			var url_details = "https://spapi.pw/api/v1/matches/" + d.guid + "/details?access_token=" + token;
			var url_stats = "https://spapi.pw/api/v1/matches/"+ d.guid + "/statistics?access_token=" + token;
			var url_actions =  "https://spapi.pw/api/v1/matches/"+ d.guid + "/actions?access_token="  + token;

			var file_details = path + name + "-" + d.guid + "-details.json";
			var file_stats = path + name + "-" + d.guid + "-statistics.json";
			var file_actions = path + name + "-" + d.guid + "-actions.json";

			if (!fs.existsSync(file_details)) {
				console.log("downloading details for " + name + " (" + d.guid + ")");
				fs.writeFileSync(file_details, sync('GET',url_details).getBody())
			}
			
			if (!fs.existsSync(file_actions)) {
				console.log("downloading actions for " + name + " (" + d.guid + ")");
				fs.writeFileSync(file_actions, sync('GET',url_actions).getBody())
			}

			if (!fs.existsSync(file_stats)) {
				console.log("downloading statistics for " + name + " (" + d.guid + ")");
				fs.writeFileSync(file_stats, sync('GET',url_stats).getBody())
			}

			var actions = JSON.parse(fs.readFileSync(file_actions, 'utf8'));
			var details = JSON.parse(fs.readFileSync(file_details, 'utf8'));
			var stats = JSON.parse(fs.readFileSync(file_stats, 'utf8'));
			
			var quick_info = {
				home: d.home_team,
				away: d.away_team,
				score: d.scores_and_stats.scores.home + ":" + d.scores_and_stats.scores.away
			}

			replacements.forEach(function (d) {
				if (quick_info.home.abbrevaition_name == d[0])
					quick_info.home.abbrevaition_name = d[1]
				if (quick_info.away.abbrevaition_name == d[0])
					quick_info.away.abbrevaition_name = d[1]
					
			})

			
			var game_data = Object.assign({}, actions, { details: details}, { statistics: stats }, { quick_info: quick_info});

			fs.writeFileSync(dist_path + name + ".json", 
				JSON.stringify(game_data), 
				function(err) {
				if(err) {
					console.log(err);
				} else {
					console.log(path + name + ".json " + "was saved!");
				}
			}); 
			
			fs.writeFileSync(prod_file,
				lzString.compressToEncodedURIComponent(JSON.stringify(game_data)), 
				function(err) {
				if(err) {
					console.log(err);
				} else {
					console.log(dist_path + name + " was saved!");
				}
			}); 
		} else {
			console.log(name + " exits");
		}
				
	});
    
    const { execSync } = require('child_process');
    execSync('cp dist/* ../../public/data/f/');



