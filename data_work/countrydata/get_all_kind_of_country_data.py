#!/usr/bin/python

import csv
import json
import requests
import time
import os
import pandas as pd
from google.cloud import bigquery

client = bigquery.Client.from_service_account_json("../fooviz-a436945f08a9.json")
 

with open("../countries.csv") as csvfile:
	reader = csv.DictReader(csvfile)

	csvout = open("countries.csv","w")	
	fields = ["fifa_code", "iso", "iso2", "name", "alt_name" , "capital", "lat", "lng", "capital_lat", "capital_lng", "ghcn_id", "ghcn_name","income_level","population","gdp"]
	writer = csv.DictWriter(csvout,fieldnames=fields)
	writer.writeheader
	
	for row in reader:

		print "get worldbank population for " + row["iso"]
		link = "http://api.worldbank.org/v2/countries/{}/indicators/SP.POP.TOTL?format=json".format(
			row["iso"], )		
		r = requests.get(link)
		row["population"] = r.json()[1][1]["value"]
		with open("pop/{}.json".format(row["iso"]), 'w') as out:
			json.dump(r.json(), out)


		print "get worldbank GDP for " + row["iso"]
		link = "http://api.worldbank.org/v2/countries/{}/indicators/NY.GDP.MKTP.CD?format=json".format(
			row["iso"], )		
		r = requests.get(link)
		row["gdp"] = r.json()[1][1]["value"]
		with open("gdp/{}.json".format(row["iso"]), 'w') as out:
			json.dump(r.json(), out)

		
		print "get worldbank description for " + row["iso"]
		link = "http://api.worldbank.org/v2/countries/{}?format=json".format(
			row["iso"], )		
		r = requests.get(link)
		b = r.json()[1][0]
		row["iso2"] = b["iso2Code"]
		row["income_level"] = b["incomeLevel"]["value"]
		
		print "get location for " + row["name"]
		link = "https://maps.googleapis.com/maps/api/geocode/json?address={}&key=AIzaSyA_ioMX1xt6UmUEbZBldKXCR1GKRwWwtY8".format(
			row["name"])
		r = requests.get(link)
		loc = r.json()["results"][0]["geometry"]["location"]
		row["lat"] = loc["lat"]
		row["lng"] = loc["lng"]
		print "it's: ", loc

		print "get location for " + row["capital"]
		link = "https://maps.googleapis.com/maps/api/geocode/json?address={},{}&key=AIzaSyA_ioMX1xt6UmUEbZBldKXCR1GKRwWwtY8".format(
			row["capital"], row["name"])
		r = requests.get(link)
		loc = r.json()["results"][0]["geometry"]["location"]
		print "it's: ", loc
		row["capital_lat"] = loc["lat"]
		row["capital_lng"] = loc["lng"]	
		
		print "find nearest station"
		
		sql = """
			CREATE TEMPORARY FUNCTION Degrees(radians FLOAT64) RETURNS FLOAT64 AS
			(
			  (radians*180)/(22/7)
			);	
						
			CREATE TEMPORARY FUNCTION RADIANS(degrees FLOAT64) AS (
			  (degrees*(22/7))/180
			);

			CREATE TEMPORARY FUNCTION DistanceKm(lat FLOAT64, lon FLOAT64, lat1 FLOAT64, lon1 FLOAT64) AS (
				 Degrees( 
				  ACOS( 
					COS( Radians(lat1) ) * 
					COS( Radians(lat) ) *  
					COS( Radians(lon1 ) -  
					Radians( lon ) ) +  
					SIN( Radians(lat1) ) *  
					SIN( Radians( lat ) ) 
					) 
				) * 111.045
			);	
			SELECT 
				id, name, DistanceKm(latitude, longitude, {},{}) as distance
			FROM 
				`bigquery-public-data.ghcn_m.ghcnm_tavg_stations` 
			ORDER BY 
				distance 
			LIMIT 3;
		""".format(loc["lat"],loc["lng"])
		
		
		df = client.query(sql).to_dataframe()
		
		print "it's: ", df.loc[0][0], df.loc[0][1], df.loc[0][2]
		print "it's: ", df.loc[1][0], df.loc[1][1], df.loc[1][2]
		print "it's: ", df.loc[2][0], df.loc[2][1], df.loc[2][2]
		
		x = pd.DataFrame(index=[],columns=[])

		for i in range(0,3):
			print "now get temperature series"
			sql = """
				SELECT value6 / 100 AS temp, year, b.id, b.name
				FROM `bigquery-public-data.ghcn_m.ghcnm_tavg` a \
				JOIN `bigquery-public-data.ghcn_m.ghcnm_tavg_stations` b \
				ON  b.id = a.id  \
				WHERE b.id = {} AND value6 != -9999 \
				ORDER BY year DESC
			""".format(df.loc[i][0])

			
			tdf = client.query(sql).to_dataframe()
			
			print "{} has {} observations".format(
				df.loc[i][1], tdf.size)
			if tdf.size > x.size:
				print "take it"
				x = tdf.copy()
	
		x.to_csv("ghcn/{}.csv".format(row["fifa_code"]))

		row["ghcn_id"] =  x.loc[0][2]
		row["ghcn_name"] =  x.loc[0][3]
		writer.writerow(row)

		print "----------------"
		time.sleep(1)

