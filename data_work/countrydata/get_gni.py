#!/usr/bin/python

import csv
import json
import requests
import time
import os
import pandas as pd
from google.cloud import bigquery

client = bigquery.Client.from_service_account_json("../fooviz-a436945f08a9.json")
 

with open("../countries.csv") as csvfile:
	reader = csv.DictReader(csvfile)

	csvout = open("countries-gni-gdp-cap.csv","w")	
	fields = ["fifa_code", "iso", "iso2", "name" , "capital", "color1", "color2", "gdp_cap","gdp_cap_ppp","gni_cap","gni_cap_ppp"]
	writer = csv.DictWriter(csvout,fieldnames=fields)
	writer.writeheader()
	
	for row in reader:

		print "get worldbank GDP per cap for " + row["iso"]
	
		
		indicator = "NY.GDP.PCAP.CD"
		link = "http://api.worldbank.org/v2/countries/{}/indicators/{}?format=json".format(
			row["iso"], indicator)		
		r = requests.get(link)
		row["gdp_cap"] = r.json()[1][1]["value"]
		print("current: {} ({})".format(row["gdp_cap"], r.json()[1][1]["date"]))
		with open("gdp_cap/{}.json".format(row["iso"]), 'w') as out:
			json.dump(r.json(), out)

		print "get worldbank GDO per cap (PPP adjusted) for " + row["iso"]
		
		
		indicator = "NY.GDP.PCAP.PP.CD"
		link = "http://api.worldbank.org/v2/countries/{}/indicators/{}?format=json".format(
			row["iso"], indicator)		
		r = requests.get(link)
		row["gdp_cap_ppp"] = r.json()[1][1]["value"]
		print("current: {} ({})".format(row["gdp_cap_ppp"], r.json()[1][1]["date"]))
		with open("gdp_cap_ppp/{}.json".format(row["iso"]), 'w') as out:
			json.dump(r.json(), out)

		print "get worldbank GNI per cap for " + row["iso"]
		
		indicator = "NY.GNP.PCAP.CD"
		link = "http://api.worldbank.org/v2/countries/{}/indicators/{}?format=json".format(
			row["iso"], indicator)		
		r = requests.get(link)
		row["gni_cap"] = r.json()[1][1]["value"]
		print("current: {} ({})".format(row["gni_cap"],  r.json()[1][1]["date"]))

		with open("gni_cap/{}.json".format(row["iso"]), 'w') as out:
			json.dump(r.json(), out)
			
		print "get worldbank GNI per cap (PPP adjusted) for " + row["iso"]


		indicator = "NY.GNP.PCAP.PP.CD"
		link = "http://api.worldbank.org/v2/countries/{}/indicators/{}?format=json".format(
			row["iso"], indicator)		
		r = requests.get(link)
		row["gni_cap_ppp"] = r.json()[1][1]["value"]
		print("current: {} ({})".format(row["gni_cap_ppp"],  r.json()[1][1]["date"]))

		with open("gni_cap_ppp/{}.json".format(row["iso"]), 'w') as out:
			json.dump(r.json(), out)

		writer.writerow(row)

		print "----------------"
		time.sleep(1)

