#!/usr/bin/python

import csv
import json
import requests
import time
import os
import pandas as pd
 

with open("../countries.csv") as csvfile:
	reader = csv.DictReader(csvfile)

	csvout = open("countries-gni-gdp-cap.csv","w")	
	fields = ["fifa_code", "iso", "iso2", "name" , "capital", "color1", "color2", "co2_cap"]
	writer = csv.DictWriter(csvout,fieldnames=fields)
	writer.writeheader()
	
	for row in reader:

		print "get worldbank C02 per cap for " + row["iso"]
	
	
		indicator = "EN.ATM.CO2E.PC"
		link = "http://api.worldbank.org/v2/countries/{}/indicators/{}?format=json".format(
			row["iso"], indicator)		
		r = requests.get(link)
	#print link
		#print  r.json()[1][3]["value"], r.json()[1][3]["date"]
		row["co2_cap"] = r.json()[1][3]["value"]
		print("current: {} ({})".format(row["co2_cap"], r.json()[1][3]["date"]))
		with open("co2_cap/{}.json".format(row["iso"]), 'w') as out:
			json.dump(r.json(), out)

		print "get worldbank C02 per cap) for " + row["iso"]
		
		
		writer.writerow(row)

		print "----------------"
		time.sleep(1)

