#!/usr/bin/python

import csv
import json
import requests
import time
import os
import pandas as pd
from google.cloud import bigquery

client = bigquery.Client.from_service_account_json("../fooviz-a436945f08a9.json")
 

with open("../countries.csv") as csvfile:
	reader = csv.DictReader(csvfile)
	
	
	for row in reader:
		
		
		print "get worldbank internet data for " + row["iso"]
		link = "http://api.worldbank.org/v2/countries/{}/indicators/2.0.cov.Int?format=json".format(
			row["iso"], )		
		r = requests.get(link)
		with open("internet/{}.json".format(row["iso"]), 'w') as out:
			json.dump(r.json(), out)

		
		
		periods = [
			[ 1920, 1939 ],
			[ 1940, 1959 ],
			[ 1960, 1979 ],
			[ 1980, 1999 ],
		]
		
		future =  [
			[ 2020, 2039 ],
			[ 2040, 2059 ],
			[ 2060, 2079 ],
			[ 2080, 2099 ],
		]
			
		
		print "get worldbank climate data for " + row["iso"]
		past = False
		for p in periods:
		
			link = "http://climatedataapi.worldbank.org/climateweb/rest/v1/country/mavg/tas/{}/{}/{}".format(
				p[0],p[1],row["iso"], )		
			r = requests.get(link)
			if past:
				past = past + r.json()
			else: 
				past = r.json()
		
		
		with open("temp-past/{}.json".format(row["iso"]), 'w') as out:
			json.dump(past, out)
			
		past = False
		for p in future:
		
			link = "http://climatedataapi.worldbank.org/climateweb/rest/v1/country/mavg/tas/{}/{}/{}".format(
				p[0],p[1],row["iso"], )		
			r = requests.get(link)
			if past:
				past = past + r.json()
			else: 
				past = r.json()
		
		with open("temp-future/{}.json".format(row["iso"]), 'w') as out:
			json.dump(past, out)
			
			
			

		print "----------------"
		time.sleep(1)

