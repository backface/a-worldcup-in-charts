from google.cloud import bigquery
client = bigquery.Client.from_service_account_json("../fooviz-a436945f08a9.json")

print "find nearest stations"

name = "MANHATTAN"
download_data = True
		
sql = """	
	SELECT 
		id, name, 	latitude, longitude
	FROM 
		`bigquery-public-data.ghcn_m.ghcnm_tavg_stations` 

	WHERE 
		name LIKE "%{}%"
	LIMIT 10;
""".format(name)

df = client.query(sql).to_dataframe()
print df
		
if download_data:
	sql = """
		SELECT value6 / 100 AS temp, year, name
		FROM `bigquery-public-data.ghcn_m.ghcnm_tavg` a \
		JOIN `bigquery-public-data.ghcn_m.ghcnm_tavg_stations` b \
		ON  b.id = a.id  \
		WHERE 
			b.name LIKE '%{}%'
		AND value6 != -9999 \
		ORDER BY year DESC
		""".format(name)

	df = client.query(sql).to_dataframe()
	df.to_csv(name.replace("/","-") + ".csv")

