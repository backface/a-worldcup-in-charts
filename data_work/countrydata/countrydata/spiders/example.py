# -*- coding: utf-8 -*-
import scrapy


class ExampleSpider(scrapy.Spider):
    name = 'example'
    allowed_domains = ['en.wikipedia.org']
    start_urls = ['https://en.wikipedia.org/wiki/Senegal']


    def parse(self, response):
		yield {
			"lat": response.css("span#coordinates span.geo-default span.latitude::text").extract_first(),
			"lon": response.css("span#coordinates span.geo-default span.longitude::text").extract_first()
		}
