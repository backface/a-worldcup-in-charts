#!/usr/bin/python

import csv
import json
import requests
import time
import os
from google.cloud import bigquery

client = bigquery.Client.from_service_account_json("../fooviz-a436945f08a9.json")
 

with open("../countries.csv") as csvfile:
	reader = csv.DictReader(csvfile)

	for row in reader:
	
		print "find nearest stations"
		
		sql = """
			CREATE TEMPORARY FUNCTION Degrees(radians FLOAT64) RETURNS FLOAT64 AS
			(
			  (radians*180)/(22/7)
			);	
						
			CREATE TEMPORARY FUNCTION RADIANS(degrees FLOAT64) AS (
			  (degrees*(22/7))/180
			);

			CREATE TEMPORARY FUNCTION DistanceKm(lat FLOAT64, lon FLOAT64, lat1 FLOAT64, lon1 FLOAT64) AS (
				 Degrees( 
				  ACOS( 
					COS( Radians(lat1) ) * 
					COS( Radians(lat) ) *  
					COS( Radians(lon1 ) -  
					Radians( lon ) ) +  
					SIN( Radians(lat1) ) *  
					SIN( Radians( lat ) ) 
					) 
				) * 111.045
			);	
			SELECT 
				id, name, DistanceKm(latitude, longitude, {},{}) as distance
			FROM 
				`bigquery-public-data.ghcn_m.ghcnm_tavg_stations` 
			ORDER BY 
				distance 
			LIMIT 10;
		""".format(row["lat"],lat["lng"])
		
		
		df = client.query(sql).to_dataframe()
		
		print df
		
		print "it's: ", df.loc[0][0], df.loc[0][1], df.loc[0][2]
		print "it's: ", df.loc[0][0], df.loc[0][1], df.loc[0][2]
		
		row["ghcn_id"] =  df.loc[0][0]
		row["ghcn_name"] =  df.loc[0][1]

		writer.writerow(row)
		
		print "now get temperature series"
		sql = """
			SELECT value6 / 100 AS temp, year, name
			FROM `bigquery-public-data.ghcn_m.ghcnm_tavg` a \
			JOIN `bigquery-public-data.ghcn_m.ghcnm_tavg_stations` b \
			ON  b.id = a.id  \
			WHERE b.id = {} AND value6 != -9999 \
			ORDER BY year DESC
		""".format(row["ghcn_id"])

		
		df = client.query(sql).to_dataframe()
		df.to_csv("ghcn2/{}.csv".format(row["fifa_code"]))

		print "----------------"
		time.sleep(1)

