from google.cloud import bigquery
client = bigquery.Client()

sql = """
	SELECT value6 / 100 AS temp, year, name
	FROM `bigquery-public-data.ghcn_m.ghcnm_tavg` a \
	JOIN `bigquery-public-data.ghcn_m.ghcnm_tavg_stations` b \
	ON  b.id = a.id  \
	WHERE b.name LIKE 'WIEN%' AND value6 != -9999 \
	ORDER BY year DESC
    """

df = client.query(sql).to_dataframe()
df.to_csv("Wien-June-Avg.csv")

