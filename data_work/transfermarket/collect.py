import requests
from bs4 import BeautifulSoup

import pandas as pd
import time
import csv
import os

headers = {'User-Agent': "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8"}


f = open("wc.html")
pageSoup = BeautifulSoup(f.read(), 'html.parser')

TeamTags = pageSoup.find_all("td", {"class": "links no-border-links hauptlink"})

Teams = {}
for t in TeamTags:	
	Teams[t.text.encode("ascii")] = int(t.find("a")["id"].encode("ascii"))

with open("../countries.csv") as csvfile:
	reader = csv.DictReader(csvfile)
	for row in reader:
		
		ofile = row["fifa_code"] + ".csv"
		if not os.path.isfile(ofile):
			
			
			id = Teams[row["name"]]
			print id, row["name"]
			page = "https://www.transfermarkt.com/{}/startseite/verein/{}/saison_id/".format(row["name"], id)		
			
			print page
			pageTree = requests.get(page, headers=headers)
			pageSoup = BeautifulSoup(pageTree.content, 'html.parser')


			#f = open("rus.html")
			#pageSoup = BeautifulSoup(f.read(), 'html.parser')

			Players = []
			PlayersContainer = pageSoup.find_all("table", {"class": "inline-table"})
			for p in PlayersContainer:
				Players.append( 
					p.find("span", {"class":"hide-for-small"}) \
					.find("a", {"class": "spielprofil_tooltip"}) \
					.text )

			Values = []
			ValueTags = pageSoup.find_all("td", {"class": "rechts hauptlink"})
			for v in ValueTags:
				Values.append(v.text \
								.replace(",","") \
								.replace(" Mill.","00000") \
								.replace(" Th.","000") \
								[:-4] )
				
			df = pd.DataFrame({"Players":Players,"Values":Values})
			print df

			df.to_csv(ofile,index=False,encoding="utf8")
			time.sleep(10)
