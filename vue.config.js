var path = require('path')
var webpack = require('webpack')
var JavaScriptObfuscator = require('webpack-obfuscator');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
// ...

// webpack plugins array
/*
  plugins: [
	new JavaScriptObfuscator ({
      rotateUnicodeArray: true
  })
],

*/
// vue.config.js
module.exports = {
  lintOnSave: false,
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      return {
        output: {
          publicPath: '/'
        },
        plugins: [
		  new JavaScriptObfuscator ({ rotateUnicodeArray: true }),
		  /*
		  new webpack.optimize.UglifyJsPlugin({
			  sourceMap: false,
			  compress: {
				warnings: false
			  }
			}),
		 */
		 
		  new UglifyJsPlugin({
            uglifyOptions:
			{
				//compress: true,
				warnings: false,
				sourceMap: false,
				//mangle: true,
				output: {
					comments: false
				},
				compress: {
					drop_console: true
				} 
			}
		  }), 
		  //new webpack.LoaderOptionsPlugin({ minimize: true })
		],
	
/*	
		optimization: {
			minimizer: [
			  // we specify a custom UglifyJsPlugin here to get source maps in production
			  new UglifyJsPlugin({
				cache: true,
				parallel: true,
				uglifyOptions: {
				  compress: true,
				  ecma: 5,
				  mangle: true
				},
				sourceMap: false
			  })
			]
		  }
*/		  
      }
    } else {
      return {
        output: {
          publicPath: '/'
        }
      }
    }
  }
}


/*
if (process.env.NODE_ENV === 'production') {
  //module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: false,
      compress: {
        warnings: false
      }
    }),
    
    new webpack.LoaderOptionsPlugin({
      minimize: true
    }),
    new JavaScriptObfuscator ({
      rotateUnicodeArray: true
  })
  ])
}
*/
